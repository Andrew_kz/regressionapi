﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using RegressionApi.Models;
using RegressionApi.Calculation;
using System.Web.Http.Cors;

namespace RegressionApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class Chart3DController : ApiController
    {
        [HttpPost]
        [Route("api/linear3dchart")]
        public IHttpActionResult Linear3DChart(BaseChartData3D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData3D chart = Calculate3DChartData.Linear3DChart(data.baseXData, data.regressionCoef, data.x1Index, data.x2Index, data.property);
            return Json(chart);
        }
        
        [HttpPost]
        [Route("api/exponential3dchart")]
        public IHttpActionResult Exponential3DChart(BaseChartData3D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData3D chart = Calculate3DChartData.Exponential3DChart(data.baseXData, data.regressionCoef, data.x1Index, data.x2Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/hyperbola3dchart")]
        public IHttpActionResult Hyperbola3DChart(BaseChartData3D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData3D chart = Calculate3DChartData.Hyperbola3DChart(data.baseXData, data.regressionCoef, data.x1Index, data.x2Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/square3dchart")]
        public IHttpActionResult Square3DChart(BaseChartData3D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData3D chart = Calculate3DChartData.Square3DChart(data.baseXData, data.regressionCoef, data.x1Index, data.x2Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/degree3dchart")]
        public IHttpActionResult Degree3DChart(BaseChartData3D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData3D chart = Calculate3DChartData.Degree3DChart(data.baseXData, data.regressionCoef, data.x1Index, data.x2Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/polynomial3dchart")]
        public IHttpActionResult Polynomial3DChart(BasePolynimialChartData3D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData3D chart = Calculate3DChartData.Polynomial3DChart(Parsing.ParsingX(data.baseXData), Parsing.ParsingY(data.regressionCoef), data.x1Index, data.x2Index, data.property, data.polynomialDegree);
            return Json(chart);
        }

    }
}