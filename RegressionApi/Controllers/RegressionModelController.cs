﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using RegressionApi.Models;
using RegressionApi.Calculation;
using System.Web.Http.Cors;

namespace RegressionApi.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RegressionModelController : ApiController
    {
        [HttpPost]
        [Route("api/linearmodel")]
        public IHttpActionResult LinearModel(BaseCalculationData data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RegressionData model = RegressionModel.linearRegression(data.baseXData, data.baseYData);
            return Json(model);

        }

        [HttpPost]
        [Route("api/degreemodel")]
        public IHttpActionResult DegreeModel(BaseCalculationData data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RegressionData model = RegressionModel.degreeResression(data.baseXData,data.baseYData);
            return Json(model);

        }

        [HttpPost]
        [Route("api/hyperbolamodel")]
        public IHttpActionResult HyperbolaModel(BaseCalculationData data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RegressionData model = RegressionModel.hyperbolaResression(data.baseXData, data.baseYData);
            return Json(model);

        }

        [HttpPost]
        [Route("api/exponentialmodel")]
        public IHttpActionResult ExponentialModel(BaseCalculationData data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RegressionData model = RegressionModel.exponentialRegression(data.baseXData, data.baseYData);
            return Json(model);

        }

        [HttpPost]
        [Route("api/squaremodel")]
        public IHttpActionResult SquareModel(BaseCalculationData data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RegressionData model = RegressionModel.squareRegression(data.baseXData, data.baseYData);
            return Json(model);

        }

        [HttpPost]
        [Route("api/polynomialmodel")]
        public IHttpActionResult PolynomialModel(BasePolynomialCalculationData data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RegressionData model = RegressionModel.polymonialRegression(data.baseXData, data.baseYData, data.polynomialDegree);
            return Json(model);

        }
    }
}