﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using RegressionApi.Models;
using RegressionApi.Calculation;
using System.Web.Http.Cors;

namespace RegressionApi.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    public class Chart2DController : ApiController
    {
        [HttpPost]
        [Route("api/linear2dchart")]
        public IHttpActionResult Linear2DChart(BaseChartData2D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData2D chart = Calculate2DChartData.Linear2DChart(data.baseXData, data.regressionCoef, data.x1Index, data.property);
            return Json(chart);
        }
        
        [HttpPost]
        [Route("api/degree2dchart")]
        public IHttpActionResult Degree2DChart(BaseChartData2D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData2D chart = Calculate2DChartData.Degree2DChart(data.baseXData, data.regressionCoef, data.x1Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/exponential2dchart")]
        public IHttpActionResult Exponential2DChart(BaseChartData2D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData2D chart = Calculate2DChartData.Exponential2DChart(data.baseXData, data.regressionCoef, data.x1Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/hyperbola2dchart")]
        public IHttpActionResult Hyperbola2DChart(BaseChartData2D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData2D chart = Calculate2DChartData.Hyperbola2DChart(data.baseXData, data.regressionCoef, data.x1Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/square2dchart")]
        public IHttpActionResult SquareDChart(BaseChartData2D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData2D chart = Calculate2DChartData.Square2DChart(data.baseXData, data.regressionCoef, data.x1Index, data.property);
            return Json(chart);
        }

        [HttpPost]
        [Route("api/polynomial2dchart")]
        public IHttpActionResult PolynomialDChart(BasePolynimialChartData2D data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChartData2D chart = Calculate2DChartData.Polynomial2DChart(data.baseXData, data.regressionCoef, data.x1Index, data.property, data.polynomialDegree);
            return Json(chart);
        }
    }
}