﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RegressionApi.Models
{
    public class BaseCalculationData
    {
        [Required(ErrorMessage ="Wrong X data")]
        public double[,] baseXData { get; set; }

        [Required(ErrorMessage = "Wrong Y data")]
        public double[] baseYData { get; set; }

    }

    public class BasePolynomialCalculationData
    {
        [Required(ErrorMessage = "Wrong X data")]
        public double[,] baseXData { get; set; }

        [Required(ErrorMessage = "Wrong Y data")]
        public double[] baseYData { get; set; }

        [Required(ErrorMessage = "Wrong polynomial degree")]
        public int polynomialDegree { get; set; }
    }
}